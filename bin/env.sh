#!/bin/sh

SETUPDIR=/local/setup/openzms

GOPATH=~/go
export GOPATH

ADMIN_TOKEN=`cat $SETUPDIR/admin_token`
export ADMIN_TOKEN
echo ADMIN_TOKEN=$ADMIN_TOKEN
ELEMENT_TOKEN=`cat $SETUPDIR/element_token`
export ELEMENT_TOKEN
echo ELEMENT_TOKEN=$ELEMENT_TOKEN

PREFIX="zms"
KIND="local-dev"
ZMS_FRONTEND_NET="${PREFIX}-frontend-${KIND}-net"
ZMS_SERVICE_NET="${PREFIX}-service-${KIND}-net"

IDENTITY_PORT_HTTP=8000
IDENTITY_IP_HTTP=`sudo docker inspect ${PREFIX}-identity-${KIND} -f '{{ index . "NetworkSettings" "Networks" "'${ZMS_FRONTEND_NET}'" "IPAddress"}}'`
IDENTITY_HTTP=http://${IDENTITY_IP_HTTP}:${IDENTITY_PORT_HTTP}/v1
export IDENTITY_HTTP
echo IDENTITY_HTTP=$IDENTITY_HTTP

IDENTITY_PORT_RPC=8002
IDENTITY_IP_RPC=`sudo docker inspect ${PREFIX}-identity-${KIND} -f '{{ index . "NetworkSettings" "Networks" "'${ZMS_FRONTEND_NET}'" "IPAddress"}}'`
IDENTITY_RPC=${IDENTITY_IP_RPC}:${IDENTITY_PORT_RPC}
export IDENTITY_RPC
echo IDENTITY_RPC=$IDENTITY_RPC

ZMC_PORT_HTTP=8010
ZMC_IP_HTTP=`sudo docker inspect ${PREFIX}-zmc-${KIND} -f '{{ index . "NetworkSettings" "Networks" "'${ZMS_FRONTEND_NET}'" "IPAddress"}}'`
ZMC_HTTP=http://${ZMC_IP_HTTP}:${ZMC_PORT_HTTP}/v1
export ZMC_HTTP
echo ZMC_HTTP=$ZMC_HTTP

ZMC_PORT_RPC=8012
ZMC_IP_RPC=`sudo docker inspect ${PREFIX}-zmc-${KIND} -f '{{ index . "NetworkSettings" "Networks" "'${ZMS_FRONTEND_NET}'" "IPAddress"}}'`
ZMC_RPC=${ZMC_IP_RPC}:${ZMC_PORT_RPC}
export ZMC_RPC
echo ZMC_RPC=$ZMC_RPC

DST_PORT_HTTP=8020
DST_IP_HTTP=`sudo docker inspect ${PREFIX}-dst-${KIND} -f '{{ index . "NetworkSettings" "Networks" "'${ZMS_FRONTEND_NET}'" "IPAddress"}}'`
DST_HTTP=http://${DST_IP_HTTP}:${DST_PORT_HTTP}/v1
export DST_HTTP
echo DST_HTTP=$DST_HTTP

DST_PORT_RPC=8022
DST_IP_RPC=`sudo docker inspect ${PREFIX}-dst-${KIND} -f '{{ index . "NetworkSettings" "Networks" "'${ZMS_FRONTEND_NET}'" "IPAddress"}}'`
DST_RPC=${DST_IP_RPC}:${DST_PORT_RPC}
export DST_RPC
echo DST_RPC=$DST_RPC

#GEOSERVER_PORT_HTTP=8025
# 8080 is container-internal port.
GEOSERVER_PORT_HTTP=8080
GEOSERVER_IP_HTTP=`sudo docker inspect ${PREFIX}-dst-geoserver-${KIND} -f '{{ index . "NetworkSettings" "Networks" "'${ZMS_FRONTEND_NET}'" "IPAddress"}}'`
GEOSERVER_HTTP=http://${GEOSERVER_IP_HTTP}:${GEOSERVER_PORT_HTTP}/geoserver/rest
export GEOSERVER_HTTP
echo GEOSERVER_HTTP=$GEOSERVER_HTTP

ALARM_PORT_HTTP=8030
ALARM_IP_HTTP=`sudo docker inspect ${PREFIX}-alarm-${KIND} -f '{{ index . "NetworkSettings" "Networks" "'${ZMS_FRONTEND_NET}'" "IPAddress"}}'`
ALARM_HTTP=http://${ALARM_IP_HTTP}:${ALARM_PORT_HTTP}/v1
export ALARM_HTTP
echo ALARM_HTTP=$ALARM_HTTP

ALARM_PORT_RPC=8032
ALARM_IP_RPC=`sudo docker inspect ${PREFIX}-alarm-${KIND} -f '{{ index . "NetworkSettings" "Networks" "'${ZMS_FRONTEND_NET}'" "IPAddress"}}'`
ALARM_RPC=${ALARM_IP_RPC}:${ALARM_PORT_RPC}
export ALARM_RPC
echo ALARM_RPC=$ALARM_RPC

export ADMIN_ELEMENT_ID=`curl -k -s -X GET -H "X-Api-Token: $ADMIN_TOKEN" -H "X-Api-Elaborate: true" "${IDENTITY_HTTP}/tokens/this" | jq -r .role_bindings[1].role_binding.element_id`
echo ADMIN_ELEMENT_ID=$ADMIN_ELEMENT_ID 

export ELEMENT_ID=`curl -k -s -X GET -H "X-Api-Token: $ELEMENT_TOKEN" -H "X-Api-Elaborate: true" "${IDENTITY_HTTP}/tokens/this" | jq -r .role_bindings[0].role_binding.element_id`
echo ELEMENT_ID=$ELEMENT_ID 

if [ -e "$SETUPDIR/rdzinrdz.sh" ];
then
    . $SETUPDIR/rdzinrdz.sh
fi
